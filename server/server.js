const express = require('express');
const keys = require('./config/index');
const path = require('path');
const cors = require('cors')
const passport = require('passport')
const PORT = 5000 || process.env.PORT;





// *** express server initialization ***
const app = express();

// getting  third party  and api middlewares and routes 
require('./routes/thirdparty/logging')();
require('./routes/index')(app);
require('./routes/thirdparty/db')();
app.use(passport.initialize())

require('./services/passport');














// // STATIC SERVER DIRECT to REACT FRONT-END IN PRODUCTION MODE . TO BUILD FOLDER 

// if(process.env.NODE_ENV === 'production') {


//     app.use(express.static('../client/build'))

//     app.get('*' , (req , res ) => {

//         res.sendFile(path.resolve(__dirname , 'client' , 'build' , 'index.html'));

//     });


// }



app.listen(process.env.PORT || 5000, () => {
    console.log('server is listening in port 5000')
}) 