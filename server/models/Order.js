const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const orderSchema = new Schema({

    username: {
        type: String
    },

    phone: {
        type: String
    },
    cartsProduct: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Cart'
    },

    delivery: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Delivery'

    },

    address: {

        town: {
            type: String
        },

        adress: {
            type: String
        }
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }




})



const Order = mongoose.model('Order', orderSchema);
exports.Order = Order; 