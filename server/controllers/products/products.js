const { Product } = require('../../models/Product');




exports.addProduct = async function (req, res) {

    // deconstruct  product's variables

    const { name, price, sku,
        category, description,
        stocks, likes, photos, details, creator, createdAt
    } = req.body;


    // add product data into Product model 
    try {

        // if there is no stocks in new product 
        // make sold out first
        if (stocks === 0) {

            details.sold = true;
        }

        const product = new Product({
            name,
            price,
            sku,
            category,
            description,
            stocks,
            likes,
            photos,
            details,
            creator: req.user._id,
            createdAt: Date.now()
        });

        await product.save();
        res.status(200).send({ success: true, product })

    } catch (error) {

        res.status(500).send({
            error: 'Error occurs during adding product'
        })

    }








}



// exports.deleteProduct = function (req, res) {


// }