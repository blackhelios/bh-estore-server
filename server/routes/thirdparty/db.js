const mongoose = require('mongoose');
const config = require('../../config/index');
const winston = require('winston')


var Fawn = require("fawn");


module.exports = function () {


    // initializing Fawn for future multiple transactions
    Fawn.init(mongoose);

    // database connecting....
    mongoose.connect(config.MONGO_URI, { useNewUrlParser: true })
        .then(() => { winston.info('connected to mongodb') })
        .catch((err) => { console.log('cannot connect to mongoDB') })


}