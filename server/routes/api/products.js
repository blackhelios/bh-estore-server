const express = require('express');
const { authMiddleware } = require('../../middleware/auth')
const { addProduct } = require('../../controllers/products/products')
const { photoUpload } = require('../../controllers/upload/photo')
const router = express.Router();



router.post('/product', authMiddleware, addProduct);
router.post('/upload', authMiddleware, photoUpload)


module.exports = router;