const passport = require('passport');
const express = require('express');
const userRoutes = require('./api/user')
const cookieParser = require('cookie-parser')
const authRoutes = require('./api/auth')
const productRoutes = require('./api/products')
const cors = require('cors')
const config = require('../config/index.js')


module.exports = function (app) {
    // third party middlewares 
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))


    const corsOptions = {
        origin: config.FRONTEND_URL,
        credentials: true
    }

    app.use(cors(corsOptions))
    app.use(passport.initialize())
    app.use(cookieParser());

    app.get('/', (req, res) => {

        res.json({ message: 'E Store api server' })
    }
    )
    // api middlewares 

    app.use('/api/v1/users', userRoutes)
    app.use('/auth', authRoutes)
    app.use('/products', productRoutes)

}